import React from "react";

const Footer = () => {
  return (
    <div className="container text-center text-md-start mt-5">
      <footer className="text-center text-lg-start bg-light text-muted">
        <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
          <div className="me-5 d-none d-lg-block">
            <span>با ما در شبکه های مجازی در ارتباط باشید : </span>
          </div>

          <div>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-facebook-f"></i>
            </a>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-twitter"></i>
            </a>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-google"></i>
            </a>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-instagram"></i>
            </a>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-linkedin"></i>
            </a>
            <a href="" className="me-4 text-reset">
              <i className="fa fa-github"></i>
            </a>
          </div>
        </section>

        <section className="">
          <div className="container text-center text-md-start mt-5">
            <div className="row mt-3">
              <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">
                  <i className="fa fa-gem me-3"></i>ساحل مارکت
                </h6>
                <p>
                  ساحل مارکت به عنوان یکی از قدیمی‌ترین فروشگاه های زنجیره ای با
                  بیش از دو دهه تجربه، با پایبندی به سه اصل، پرداخت در محل، ۷
                  روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا موفق شده تا همگام
                  با فروشگاه‌های معتبر ایران به یکی ازبزرگ‌ترین فروشگاه های شهر
                  کرج تبدیل شود. به محض ورود به سایت ساحل مارکت با دنیایی از
                  کالا رو به رو می‌شوید! هر آنچه که نیاز دارید و به ذهن شما خطور
                  می‌کند در اینجا پیدا خواهید کرد.
                </p>
              </div>

              <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">محصولات</h6>
                <p>
                  <a href="#!" className="text-reset">
                    کالا های اساسی و خواربار
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    محصولات پروتئینی
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    تنقلات
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    نوشیدنی
                  </a>
                </p>
              </div>

              <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                <h6 className="text-uppercase fw-bold mb-4">راهنمای خرید </h6>
                <p>
                  <a href="#!" className="text-reset">
                    نحوه ثبت سفارش
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    رویه ارسال سفارش
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    شیوه های پرداخت
                  </a>
                </p>
                <p>
                  <a href="#!" className="text-reset">
                    پاسخ به پرسش های متداول
                  </a>
                </p>
              </div>

              <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                <h6 className="text-uppercase fw-bold mb-4">ارتباط با ما </h6>
                <p>
                  <i className="fa fa-home me-3"></i> البرز ، کرج
                </p>
                <p>
                  <i className="fa fa-envelope me-3"></i>
                  sahel@market.com
                </p>
                <p>
                  <i className="fa fa-phone me-3"></i> 22 11 250 263 98 +
                </p>
                <p>
                  <i className="fa fa-print me-3"></i> 33 11 250 263 98 +
                </p>
              </div>
            </div>
          </div>
        </section>

        <div
          className="text-center p-4"
          style={{ backgroundColor: "rgba(0, 0, 0, 0.05)" }}
        >
          © 2021 Copyright:
          <a className="text-reset fw-bold" href="https://mdbootstrap.com/">
            MDBootstrap.com
          </a>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
