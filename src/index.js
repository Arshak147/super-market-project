import React from 'react';
import ReactDom from 'react-dom';
import App from './app'; 
import '../node_modules/font-awesome/css/font-awesome.min.css'; 

ReactDom.render(<App/> , 
    document.querySelector('#root'))