import React, { useState, useEffect } from "react";
import "../../styles/details.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
} from "react-router-dom";
import axios from "axios";
const Detail = () => {
  let { id } = useParams();
  const [productDetail, setProductDetail] = useState({});
  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/products/${id}`)
      .then((res) => {
        console.log(res.data);
        setProductDetail(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-4">
          <div class="card">
            <img
              class="card-img-top"
              src={productDetail.image}
              alt="Card image cap"
            />
            <div class="card-body">
              <p class="card-text">
                <div className="d-flex justify-content-around">
                  <i className="fa fa-heart"></i>
                  <i className="fa fa-bell"></i>
                  <i className="fa fa-share-alt"></i>
                  <i className="fa fa-line-chart"></i>
                </div>
              </p>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <h4>{productDetail.title}</h4>
          <i className="fa fa-star">{productDetail.rate}</i>
          <div className="options" style={{ marginTop: " 32px" }}>
            <h6> ویژگی ها</h6>
            <ul>
              <li>دسته بندی : {productDetail.category}</li>
              <li>نوع : {productDetail.type}</li>
              <li>توضیحات: {productDetail.detail}</li>
            </ul>
          </div>
        </div>
        <div className="col-md-4">
          <div className="basket">
            <div className="info">
              <p>فروشنده : ساحل مارکت</p>
              <i className="fa fa-shield">گارانتی اصالت و سلامت فیزیکی کالا </i>
              <br />
              <i className="fa fa-check">موجود در انبار</i>
            </div>
            <div className="price">
              <p>{productDetail.price} تومان</p>
            </div>
            <button
              className="btn btn-danger"
              style={{
                width: "100%",
                marginTop: "24px",
              }}
            >
              افزودن به سبد خرید
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Detail;
