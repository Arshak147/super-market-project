import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const Navbar = () => {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a href="/" className="navbar-brand" className="description">
            سوپرمارکت
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a href="/" className="nav-link">
                  خانه
                </a>
              </li>
              <li className="nav-item">
                <a href="/grocery" className="nav-link">
                  کالا های اساسی و خواربار
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link">محصولات پروتئینی</a>
              </li>
              <li className="nav-item">
                <a className="nav-link">تنقلات</a>
              </li>
              <li className="nav-item">
                <a className="nav-link">نوشیدنی</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </Router>
  );
};

export default Navbar;
