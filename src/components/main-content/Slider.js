import React, { useState, useEffect } from "react";
import SimpleImageSlider from "react-simple-image-slider";

import "../../styles/slickdemo.css";

const images = [
  { url: "http://www.tabiat-product.com/images/SLIFE.jpg" },
  { url: "https://blog.okcs.com/wp-content/uploads/2019/12/MILKKK.jpg" },
  { url: "https://mostafavisaffron.com/img/slider/2.jpg" },
  { url: "http://www.medyagroup.net/wp-content/uploads/2018/07/icm2.jpg" },
];

const Slider = () => {
  const size = useWindowSize();

  return (
    <div className="container-fluid">
      <div className="row slider">
        <div className="col-md-1"></div>
        <div className="col-md-10">
          <SimpleImageSlider
            width={size.width - 300}
            height={500}
            images={images}
            showBullets={true}
            showNavs={true}
          />
        </div>
      </div>
    </div>
  );
};
// Hook
function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}
export default Slider;
