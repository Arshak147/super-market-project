import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
} from "react-router-dom";

const Product = (props) => {
  const [counter, setCounter] = useState(0);

  const clickCounter = () => {
    setCounter(counter + 1);
  };

  return (
    <div className="col-md-3">
      <div className="card">
        <Link
          to={`/detail/${props.id}`}
          style={{ color: "inherit", textDecoration: "inherit" }}
        >
          <img
            className="card-img-top"
            src={props.image}
            alt="Card image cap"
          />
        </Link>
        <div className="card-body">
          <h5 className="card-title">{props.title}</h5>
          <p className="card-text" style={{ fontWeight: "bolder" }}>
            {props.price} تومان
          </p>
          <i className="fa fa-star">{props.rate}</i>
          <button
            className="btn btn-danger"
            style={{ float: "left" }}
            onClick={clickCounter}
          >
            <i className="fa fa-plus">{counter}</i>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Product;
