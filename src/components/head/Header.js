import React from 'react';
import '../../styles/Header.css';
import Navbar from '../head/Navbar'
import Login from '../head/Login'

  
const Header = () => {
    return(
      <div className="container-fluid">
      <Login />
      <Navbar />
      </div>
      
    )};

export default Header ;