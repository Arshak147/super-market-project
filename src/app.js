import React from "react";
import Header from "./components/head/Header";
import Main from "./components/main-content/Main";
import Footer from "./components/footer/Footer";
import AllProducts from "./components/products/AllProducts";
import Detail from "./components/main-content/Detail";
import Grocery from "./components/products/Grocery";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const App = () => {
  return (
    <div>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Main}>
            <Main />
          </Route>
          <Route path="/products">
            <AllProducts />
          </Route>
          <Route path="/detail/:id">
            <Detail />
          </Route>
          <Route path="/grocery">
            <Grocery />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </div>
  );
};

export default App;
