import React from "react";
import Slider from "./Slider";
import Product from "./Product";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";

const Main = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8080/api/products")
      .then((res) => {
        setProducts(res.data);
        console.log(products);
      })
      .catch((err) => console.log(err));
  }, []);

  const renderList = products.slice(0, 8).map((product) => {
    return (
      <Product
        key={product.id}
        title={product.title}
        price={product.price}
        rate={product.rate}
        image={product.image}
        id={product._id}
      />
    );
  });

  return (
    <div>
      <Slider />
      <div className="container">
        <div className="row">
          <h4 style={{ margin: "revert" }}>آخرین محصولات</h4>
          {renderList}
          <div className="col-md-4"></div>
          <div className="col-md-4"></div>
          <div className="col-md-4 mt-4">
            <Link to="/products">
              <button
                className="btn btn-outline-primary "
                style={{ float: "left" }}
              >
                مشاهده همه محصولات
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
