import React, { useState, useEffect } from "react";
import Product from "../main-content/Product";
import axios from "axios";

const Grocery = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8080/api/products/filter/")
      .then((res) => {
        setProducts(res.data);
        console.log(res.data);
      })
      .catch((err) => console.log(err));
  }, []);
  const renderList = products.map((product) => {
    return (
      <Product
        key={product.id}
        title={product.title}
        price={product.price}
        rate={product.rate}
        image={product.image}
        id={product._id}
      />
    );
  });
  return (
    <div style={{ padding: "4px" }}>
      <div className="row">{renderList}</div>
    </div>
  );
};

export default Grocery;
