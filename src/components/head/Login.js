import React from "react";
import "../../styles/Login.css";

const Login = () => {
  return (
    <div className="container-fluid">
      <div className="row login">
        <div className="col-sm-10">
          <span style={{ fontSize: "larger" }}>ساحل مارکت</span>
        </div>
        <div className="col-sm-2">
          <button className="btn btn-light">ورود / ثبت نام</button>
          <button className="btn shopping">
            <i className="fa fa-shopping-cart"></i>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
